import { AppService } from './app.service';
import { Component, OnInit } from '@angular/core';

import { Task } from './task';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular7todolist';
  tasks: Task[];
  task: Task;
  myTask = '';
  editTask = false;
  taskStatus = false;

  public constructor(private appService: AppService) {
    this.task = new Task();
  }

  ngOnInit(): void {
    this.appService.getTasks().subscribe(
      a => {
        this.tasks = a;
        console.log('Tasks', a);
        this.tasks.forEach(t => this.task = t);
      }
    );
    //this.clear();
  }

  clear(): void {
    this.myTask = '';
    this.taskStatus = false;
    console.log('clear', this.task);
  }

  submit(id) {
    if (this.myTask === '' || this.myTask === undefined) {
    } else if (this.editTask) {
       this.update(id); // By pasing the task will update that particular task
    } else {
      alert('add task');
      this.addTask();
    }
  }

  addTask(): void {
    this.task.title = this.myTask;
    this.task.completed = this.taskStatus;
    console.log('add task', this.task);
    this.appService.addTask(this.task);
    this.clear();
  }

  fillIt(task) {
    this.editTask = true;
    this.myTask = task.title;
    this.taskStatus = task.completed;
  }

  update(id) {
    this.task.title = this.myTask;
    this.task.completed = this.taskStatus;
    this.task.id = id;
    console.log('udapte', this.task);
    this.appService.update(this.task);
    this.editTask = false;
    this.clear();
  }

  delete(id) {
    this.appService.delete(id);
  }
}

