import { Task } from './task';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  basePath = '/tasks';
  taskCollection: AngularFirestoreCollection<Task>;
  tasks: Observable<Task[]>;

  constructor(private firestore: AngularFirestore) {
    this.taskCollection = this.firestore.collection(this.basePath);
    this.tasks = this.firestore.collection(this.basePath).snapshotChanges().pipe(
      map(changes => {
        return changes.map(a => {
          const data = a.payload.doc.data() as Task;
          data.id = a.payload.doc.id;
          return data;
        });
      })
    );
  }

  getTasks() {
   // return this.taskList.valueChanges(); // value changes display the data from the firestore database not the snapshot
    return this.tasks;
  }

  addTask(task: Task) {
    this.taskCollection.add({ ...task });  // by using add method it auto generates ID in firestore

    //  this.firestore.collection(this.basePath).doc("LA").set({   // use this method to manually set the doc ID
    //   title: task.title,
    //   completed: task.completed
    // });
  }

  update(task: Task) {
    this.taskCollection.doc(task.id).update(task);
  }

  delete(id) {
    this.taskCollection.doc(id).delete();
  }
}
