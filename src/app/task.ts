import { firebase} from './firebase';
export interface ITask {
    id: string;
    completed: boolean;
    createdAt: object;
    title: string;
}

export class Task implements ITask {
    id;
    completed = false;
    createdAt = firebase.database.ServerValue.TIMESTAMP;
    title;
}
