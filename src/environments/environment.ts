export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB683nlrKBxZBAM0PAT-_bhwlpm6dqwaNc',
    authDomain: 'personal-checklist.firebaseapp.com',
    databaseURL: 'https://personal-checklist.firebaseio.com',
    projectId: 'personal-checklist',
    storageBucket: 'personal-checklist.appspot.com',
    messagingSenderId: '209981653763'
  }
};
